#!/usr/bin/env node

const path = require('path');
const fs = require('fs');

const yargs = require('yargs');
const { hideBin } = require('yargs/helpers')

const split2 = require('split2');
const through2 = require('through2')

const R = require('ramda');
const { exit } = require('process');
const Graph = require('@tangle/graph')
const Table = require('cli-table3');
const colors = require('colors');


const DATA_DIR = path.resolve(__dirname, 'data');
const log = (...args) => console.log(...args);


// Inspired by https://stackoverflow.com/a/54470906/3318073, with minor edits
function deepSearchItems(object, key, predicate=((k,v) => true)) {
  let ret = [];
  if (object.hasOwnProperty(key) && predicate(key, object[key]) === true) {
    ret = [...ret, object[key]];
  }
  if (Object.keys(object).length) {
    for (let i = 0; i < Object.keys(object).length; i++) {
      let value = object[Object.keys(object)[i]];
      if (typeof value === "object" && value != null) {
        let o = deepSearchItems(object[Object.keys(object)[i]], key, predicate);
        if (o != null && o instanceof Array) {
          ret = [...ret, ...o];
        }
      }
    }
  }
  return ret;
}

async function getFhirResource(FileName, validFiles, dir=DATA_DIR) {
  let results = {};
  return new Promise((resolve, reject) => {
    fs.createReadStream(path.resolve(dir, `${FileName}.ndjson`))
      .pipe(split2())
      .pipe(through2.obj(async function(chunk, enc, callback) {
        const data = JSON.parse(chunk);
        const uid = data.id;
        
        // Record doesn't reference anything else so move on. This is a silly optimization.
        if (!chunk.includes(`"reference":`)) {
          // console.log(FileName, uid, `doesn't include any references, skipping`)
          this.push([uid, []]);
          return callback();
        }

        const references = deepSearchItems(data, 'reference');  
        const filteredRefs = references.filter(r => r && validFiles && validFiles.includes(r.split('/')[0]));
        this.push([uid, filteredRefs])
        return callback()
      }))
      .on('data', ([uid, references]) => {
        results[`${FileName}/${uid}`] = R.uniq(
          results[`${FileName}/${uid}`] ? 
            results[`${FileName}/${uid}`].concat(references) : 
            references
        );
      })
      .on("error", (error) => {
        reject(error);
      })
      .on('end', () => {
        // console.log('end', FileName, Object.keys(results).length);
        resolve(results);
      });
  });
}

async function getUserResource({firstname, lastname, id}, patientFile=path.resolve(DATA_DIR, `Patient.ndjson`)) {
  return new Promise((resolve, reject) => {
    let found = false;
    fs.createReadStream(patientFile)
      .pipe(split2())
      .pipe(through2.obj(async function(chunk, enc, callback) {
        const data = JSON.parse(chunk);
        const uid = data.id;
        
        if (id && id !== uid) return callback();

        const nameRecord = [].concat(data.name).filter(n => n.use === 'official')[0];
        const name = [
          [].concat(nameRecord.family).join('_'), 
          [].concat(nameRecord.given).join('_')
        ];

        if ((id && id === uid)|| name.join(',').toLowerCase() === `${lastname},${firstname}`.toLowerCase()) this.push({
          uid,
          id: `Patient/${uid}`,
          lastname: name[0],
          firstname: name[1]
        });

        return callback()
      }))
      .on('data', function(user) {
        found = user;
        // console.log('data', user);
      })
      .on("error", (error) => {
        reject(error);
      })
      .on('end', () => {
        if (!found) return reject(`No Patient found matching id ${id} or name ${lastname},${firstname}`)
        resolve(found);
      });
  });
}

// const test1 = {id :'ebf9231d-6a1f-432a-90c2-bc1b340ae047'};
// const test2 = {lastname: 'Treutel973', firstname: 'Mohamed943'}

async function main({firstname, lastname, id}={}, dataDir=DATA_DIR) {
  const user = await getUserResource({firstname, lastname, id})

  const table1 = new Table()
  table1.push(
    {'Patient Name': `${user.lastname}, ${user.firstname}`},
    {'Patient ID': user.uid}
  );

  log(table1.toString());


  log();
  log(`Loading records...`);
  const files = fs.readdirSync(dataDir)
    .filter(f => f.includes('.ndjson'))
    .map(f => path.basename(f, '.ndjson'));



  const nodeIds = await Promise.all(files.map(f => getFhirResource(f, files, dataDir))).then(arr => Object.assign(...arr))
  log(`Records loaded. ${Object.values(nodeIds).reduce((n, arr) => n+arr.length, 0)} found.`);
  log();


  /* 
    Begin DAG magic. 
    I mean, it's a graph problem. The library doesn't really matter, in this case I'm using @tangle/graph cause I liked their API.
    A fancier way to do this involves not filling up a nodeIds obj (which could get too big), but instead adding in your nodes to the graph when you come across them (e.g. in getFhirResource). 
    And if you wanted to get even more fancy, you could use something like RedisGraph, a graph lib wrapper for redis. 
  */

  // Setting the root node.
  nodeIds[user.id] = null
  
  // Converting to the format the library wants.
  const nodes = R.toPairs(nodeIds).map(([key,previous]) => ({key, previous}));

  // Initiating the graph;
  const graph = new Graph(nodes);

  // And querying who's connected to the root patient node
  const links = graph.getLinks(user.id)

  /* End DAG magic */


  // Just a little helper to make it cleaner to output
  const prepResult = links.reduce((obj, ref) => {
    const [f,id] = ref.split('/');
    obj[f] = (obj[f] || []).concat(id);
    return obj;
  }, {})


  const result = Object.keys(prepResult)
    .map(k => [k, prepResult[k].length])
    .sort((a,b) => (b[1] - a[1]) || a[0].localeCompare(b[0]))
    // .map(([k,l]) => `${k}: ${l}`)


  const table = new Table({
    head: ['Resource Type          ', 'Count'],
    style: { 'padding-left': 0, 'padding-right': 0 }
  });

  table.push(...result)
  log(table.toString());

  // fs.writeFileSync('tmp/nodes.json', JSON.stringify({nodes,links, result},null,2));
  exit(0)
}

const argv = yargs(hideBin(process.argv))

  .option('id', {
    type: 'string',
    description: 'Patient id'
  })
  .option('firstname', {
    alias: 'f',
    type: 'string',
    description: 'Patient first name (must be used alongside lastname)'
  })
  .option('lastname', {
    alias: 'l',
    type: 'string',
    description: 'Patient last name (must be used alongside firstname)'
  })
  .option('data-dir', {
    alias: 'd',
    type: 'string',
    default: DATA_DIR,
    description: 'Data directory to pull FHIR resources from.'
  })
  .check((argv, options) => {
    if (argv.lastname && !argv.firstname || !argv.firstname && argv.lastname) {
      throw new Error("Invalid arguments: If you're gonna pass either a firstname or a lastname, you must also pass the other.");
      return false;
    }
    return true;
  })
  .parse()


main(argv, argv.dataDir)
  .then(() => exit(0))
  .catch(e => {
    console.error(colors.red('ERROR: ' + e));
    exit(1)
  });

